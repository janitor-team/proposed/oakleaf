BEGIN {
    while(getline < "head.shtml")
	print;
}
{
    # Removes lines with <html>, <body>, or DOCTYPE tags
    if( /<\/?html.*>/ || /<\/?body.*>/ || /DOCTYPE/) {
	gsub("<","[");
	gsub(">","]");
	#print "<!-- " $0 "-->";
    }
    # all others content
    else {
	print;
    }
}
END {
    while(getline < "foot.shtml")
	print;
}
