

program testrfun

  use rfun

  implicit none

  integer, parameter :: rp = selected_real_kind(15)

  integer :: i
  real(rp) :: x, dx = 1e-5_rp

  do i = -100,100
     x = i/10.0
!     write(*,*) x,huber(x),dhuber(x),tukey(x),dtukey(x)
     write(*,*) x,dhuber(x),(huber(x+dx)-huber(x-dx))/(2*dx), &
          dtukey(x),(tukey(x+dx)-tukey(x-dx))/(2*dx), &
          tukey(x),(itukey(x+dx)-itukey(x-dx))/(2*dx)
  end do

end program testrfun
