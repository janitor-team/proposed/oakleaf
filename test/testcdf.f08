
program cdf

  use oakleaf
  use noise
  use toolbox

  implicit none

  integer, parameter :: dbl = selected_real_kind(15)
  integer, parameter :: ndata = 1000
  real(dbl), dimension(ndata) :: x,y,z,xcdf,pcdf
  integer :: i

  call gnoise_fill(0.0_dbl,1.0_dbl,x)
  call gnoise_fill(0.0_dbl,100.1_dbl,y)
  call gnoise_fill(1.0_dbl,0.01_dbl,z)

  do i = 1, ndata,5
     x(i) = y(i)
  end do

  do i = 5, ndata,5
     x(i) = z(i)
  end do


  call ecdf(x,xcdf,pcdf)

  open(1,file='/tmp/cdf')
  do i = 1, size(xcdf)
     write(1,*) xcdf(i),pcdf(i)
  end do
  close(1)

end program cdf
