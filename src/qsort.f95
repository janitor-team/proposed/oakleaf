!
!  QUICKSORT - recursive version of the QuickSort algorithm
!
!  by  Wirth,N: Algorithm + Data Structure = Programs, Prentice-Hall, 1975
!
!  Copyright © 1997-2011, 2017 F.Hroch (hroch@physics.muni.cz)
!
!  This file is part of OakLeaf.
!
!  OakLeaf is free software: you can redistribute it and/or modify
!  it under the terms of the GNU Lesser General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.
!
!  OakLeaf is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU Lesser General Public License for more details.
!
!  You should have received a copy of the GNU Lesser General Public License
!  along with OakLeaf.  If not, see <http://www.gnu.org/licenses/>.


module quicksorter

  implicit none
  private

  ! precision of real numbers
  integer, parameter, private :: rp = selected_real_kind(15)

  private :: trid
  public :: qsort

contains

  subroutine qsort(a)

    real(rp), dimension(:), intent(in out) :: a
    integer :: n

    n = size(a)
    if( n < 2 ) return

    call trid(1,n,a)

  end subroutine qsort


  recursive subroutine trid(l,r,a)

    integer,intent(in) :: l,r
    real(rp), dimension(:),intent(in out) :: a

    integer :: i,j
    real(rp) :: x,w     ! internal buffers

    i = l
    j = r
    x = a((l+r)/2)

    do
       do while ( a(i) < x )
          i = i + 1
       end do

       do while ( x < a(j) )
          j = j - 1
       end do

       if( i <= j )then
          w = a(i)
          a(i) = a(j)
          a(j) = w
          i = i + 1
          j = j - 1
       endif

       if( i > j ) exit
    end do

    if( l < j ) call trid(l,j,a)
    if( i < r ) call trid(i,r,a)

  end subroutine trid

end module quicksorter
